# Build systems all the way down

## Track
Embedded, Mobile and Automotive devroom

https://lists.fosdem.org/pipermail/fosdem/2021q4/003334.html

## Title
Build Systems all the way down.

## Subtitle
Untangling OpenEmbedded/meta-zephyr/zephyr integration.

## Event type
20 min talk + 5 min QA

## Persons
Eilís "pidge" Ní Fhlannagáin

Pidge has been involved in Linux, Open Source and embedded systems for over 30
years. She has been working on and around the Yocto Project and OpenEmbedded
since before v1.0. She is the former Release manager for the Yocto Project
and is one of the current maintainers of Oniro. She was the original team lead
for the team who did the original work on meta-zephyr. She has previously
spoken on various topics at multiple conferences, ranging from IoT security
to Legal compliance issues around open source technologies.

## Abstract (one paragraph)

In this talk, pidge will take a critical look at the places where meta-zephyr
succeeds and fails in its original goals, the reasons behind that and the 
steps being taken to fix those issues.

## Description

OpenEmbedded Zephyr and meta-zephyr are powerful tools that can give IoT
developers the ability to rapidly develop embedded solutions. Integrating 
them all however is challenging, made more so by wrapping multiple build 
systems, machine configurations and build languages into a coherent solution.

In this talk, pidge is going to show changes being implemented through her work
on the Oniro project to the meta-zephyr layer that bring most zephyr supported
machine configurations into meta-zephyr but also removes the need for OE machine 
configurations to have any knowledge of core zephyr modules. She will discuss
the issues she encountered around integating those changes from the zephyr side.

## Links
A previous presentation covering the more general topic of security in the
distribution at Embedded Linux Conference 2021
https://www.youtube.com/watch?v=u6Xo0QF6AMQ

Oniro project and its security tooling is available from
https://booting.oniroproject.org/
